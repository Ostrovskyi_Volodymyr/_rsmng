CFLAGS:=-fno-builtin -mthumb -D_STM32F1xx_
_ROOT_DIR=..
_CFLAGS=${CFLAGS} -I${_ROOT_DIR}/include
PREFIX=arm-none-eabi-
LD=${PREFIX}gcc
CC=${PREFIX}gcc
OBJCOPY=${PREFIX}objcopy
AR=${PREFIX}ar

all: image

image: bindir dependencies
	${LD} -fno-builtin -nostartfiles -nodefaultlibs -nostdlib -nolibc -Tsrc/hal/boot/ldscripts/main.ld bin/startup.o bin/main.o -Lbin/ -lkernel -lhal -lstd -o bin/firmware.elf
	${OBJCOPY} -O binary bin/firmware.elf bin/firmware.bin

flash: image
	openocd -f board/stm32f103c8t6.cfg -c "program bin/firmware.bin verify reset exit 0x08000000"

dependencies:
	make -C src ROOT_DIR=${_ROOT_DIR} CFLAGS="${_CFLAGS}" CC=${CC} AR=${AR} 

bindir:
	mkdir -p bin