#include <stdlib/string.h>

void* memset(void *ptr, unsigned char value, unsigned int num) {
	unsigned char *cptr = (unsigned char*)ptr;
	while (num --> 0) {
		*cptr = value;
		cptr++;
	}
	return ptr;
}