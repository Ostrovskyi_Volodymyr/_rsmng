#include <hal/systick.h>

void hal_systick_enable(unsigned char intr) {
	systick->ctrl |= 1;
	if (intr) {
		systick->ctrl |= 2;
	}
}

void hal_systick_set(unsigned int value) {
	systick->load = value & 0xFFFFFF;
}

void hal_systick_source(int src) {
	systick->ctrl |= src << 2;
}

unsigned int hal_systick_get(void) {
	return systick->current;
}