extern void *_estack, *_sidata, *_sdata, *_edata, *_sbss, *_ebss;

void startup_main(void);

void __attribute__((naked, noreturn)) irq_reset_handler() {
	void **data_in_flash, **ram;
	
	for (data_in_flash = &_sidata, ram = &_sdata; ram != &_edata; data_in_flash++, ram++) {
		*ram = *data_in_flash;
	}

	for (ram = &_sbss; ram != &_ebss; ram++) {
		*ram = 0;
	}
    startup_main();
	while(1);
}

void __attribute__((naked, noreturn)) irq_default_handler() {
    while(1);
}

#define WEAK_INT_HANDLER(funcname) void funcname() __attribute__((weak, alias("irq_default_handler")));

//#ifdef _STM32F1xx_
WEAK_INT_HANDLER(irq_nmi_handler)
WEAK_INT_HANDLER(irq_hardfault_handler)
WEAK_INT_HANDLER(irq_memmanage_handler)
WEAK_INT_HANDLER(irq_busfault_handler)
WEAK_INT_HANDLER(irq_usagefault_handler)
WEAK_INT_HANDLER(irq_syscall_handler)
WEAK_INT_HANDLER(irq_debugmonitor_handler)
WEAK_INT_HANDLER(irq_pendsysservice_handler)
WEAK_INT_HANDLER(irq_systick_handler)

void *vectors[] __attribute__((section(".isr_vector"), used)) = {
    &_estack,
	&irq_reset_handler,
	&irq_nmi_handler,
	&irq_hardfault_handler,
	&irq_memmanage_handler,
	&irq_busfault_handler,
	&irq_usagefault_handler,
	0, 0, 0, 0,
	&irq_syscall_handler,
	&irq_debugmonitor_handler,
	0,
	&irq_pendsysservice_handler,
	&irq_systick_handler,
};
/*#else
void *vectors[] __attribute__((section(".isr_vector"), used)) = {
    &_estack,
	&irq_reset_handler,
};
#endif // _STM32F1xx_*/