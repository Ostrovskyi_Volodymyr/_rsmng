#include <hal/rcc.h>

static struct {
	int mul, div, src;
} PLL_SOURCE;

static int SYSCLK_SOURCE = HAL_SYSCLK_SRC_HSI;
static int HCLK_PRESCALER = HAL_HCLK_DIV_1;

void hal_rcc_pll_config(pll_config_t *cfg) {
	if (rcc->ctrl[25] != 0)
		return;
	rcc->cfg[18] = cfg->mul & 0x1;
	rcc->cfg[19] = (cfg->mul & 0x2) >> 1;
	rcc->cfg[20] = (cfg->mul & 0x4) >> 2;
	rcc->cfg[21] = (cfg->mul & 0x8) >> 3;
	rcc->cfg[17] = cfg->hse_div;
	rcc->cfg[16] = cfg->src;
	PLL_SOURCE.mul = cfg->mul;
	PLL_SOURCE.src = cfg->src;
	PLL_SOURCE.div = cfg->hse_div;
}

void hal_rcc_pll_enable() {
	rcc->ctrl[24] = 1;
	while (rcc->ctrl[25] != 1);
}

void hal_rcc_sysclk_source(int src) {
	int l = src & 0x1, h = (src & 0x2) >> 1;
	rcc->cfg[0] = l;
	rcc->cfg[1] = h;
	SYSCLK_SOURCE = src;
	while (rcc->cfg[2] != l || rcc->cfg[3] != h);
}

static unsigned int _pll_freq_func(void);
static unsigned int _hse_freq_func(void);
static unsigned int _hsi_freq_func(void);

static const unsigned int (*freq_funcs[3])(void) = {
	[HAL_SYSCLK_SRC_HSI] = &_hsi_freq_func,
	[HAL_SYSCLK_SRC_PLL] = &_pll_freq_func,
	[HAL_SYSCLK_SRC_HSE] = &_hse_freq_func,
};

unsigned int hal_rcc_hclk_freq(void) {
	if (freq_funcs[SYSCLK_SOURCE] != 0) {
		int presc = 0;
		switch (HCLK_PRESCALER) {
			case HAL_HCLK_DIV_1: presc = 1; break;
			case HAL_HCLK_DIV_2: presc = 2; break;
			case HAL_HCLK_DIV_4: presc = 4; break;
			case HAL_HCLK_DIV_8: presc = 8; break;
			case HAL_HCLK_DIV_16: presc = 16; break;
			case HAL_HCLK_DIV_64: presc = 64; break;
			case HAL_HCLK_DIV_128: presc = 128; break;
			case HAL_HCLK_DIV_256: presc = 256; break;
			case HAL_HCLK_DIV_512: presc = 512; break;
			default: presc = 1; break;
		}
		return freq_funcs[SYSCLK_SOURCE]() / presc;
	}
	return 0;
}

static unsigned int _pll_freq_func(void) {
	int mul = 0;
	switch (PLL_SOURCE.mul) {
		case HAL_PLL_MUL_2: mul = 2; break;
		case HAL_PLL_MUL_3: mul = 3; break;
		case HAL_PLL_MUL_4: mul = 4; break;
		case HAL_PLL_MUL_5: mul = 5; break;
		case HAL_PLL_MUL_6: mul = 6; break;
		case HAL_PLL_MUL_7: mul = 7; break;
		case HAL_PLL_MUL_8: mul = 8; break;
		case HAL_PLL_MUL_9: mul = 9; break;
		case HAL_PLL_MUL_10: mul = 10; break;
		case HAL_PLL_MUL_11: mul = 11; break;
		case HAL_PLL_MUL_12: mul = 12; break;
		case HAL_PLL_MUL_13: mul = 13; break;
		case HAL_PLL_MUL_14: mul = 14; break;
		case HAL_PLL_MUL_15: mul = 15; break;
		case HAL_PLL_MUL_16: mul = 16; break;
		default: mul = 1;
	}
	int source_freq = 1;

	if (PLL_SOURCE.src == HAL_PLL_SRC_HSI) {
		source_freq = _hsi_freq_func() / 2;
	} else if (PLL_SOURCE.src == HAL_PLL_SRC_HSE) {
		if (PLL_SOURCE.div == HAL_PLL_HSE_DIV_1) {
			source_freq = _hse_freq_func();
		} else if (PLL_SOURCE.div == HAL_PLL_HSE_DIV_2) {
			source_freq = _hse_freq_func() / 2;
		}
	}
	return source_freq * mul;
}

static unsigned int _hse_freq_func(void) {
	return HSE_CLK_FREQ;
}

static unsigned int _hsi_freq_func(void) {
	return HSI_CLK_FREQ;
}