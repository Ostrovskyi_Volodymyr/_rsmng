#include <hal/gpio.h>
// Enable APB clock for gpio
#include <hal/rcc.h>

static gpio_base_t *gpios[GPIO_PORT_COUNT];

void hal_gpio_init(void) {
	int i = 0;
	for (; i < GPIO_PORT_COUNT; ++i) {
		gpios[i] = (gpio_base_t*)PERIF_ALIAS_ADDR(GPIO_BASE + GPIO_OFF*i);
	}
}

gpio_base_t* hal_gpio_port(int port) {
	if (port < GPIO_PORT_COUNT) {
		return gpios[port];
	}
	return 0;
}

void hal_gpio_enable_port(int port) {
	if (port < GPIO_PORT_COUNT) {
		rcc->apb2_en[port + 2] = 1;
	}
}

void hal_gpio_disable_port(int port) {
	if (port < GPIO_PORT_COUNT) {
		rcc->apb2_en[port + 2] = 0;
	}
}

int hal_gpio_port_enabled(int port) {
	if (port < GPIO_PORT_COUNT) {
		return rcc->apb2_en[port + 2];
	}
	return 0;
}

void hal_gpio_config(gpio_base_t *port, int pin, int mode) {
	port->moder[pin].cnf[0] = (mode & 0b0100) >> 2;
	port->moder[pin].cnf[1] = (mode & 0b1000) >> 3;
	port->moder[pin].mode[0] = mode & 0b0001;
	port->moder[pin].mode[1] = (mode & 0b0010) >> 1;
}

void hal_gpio_write(gpio_base_t *port, int pin, int val) {
	port->set_pin[pin] = val;
}

int hal_gpio_read(gpio_base_t *port, int pin) {
	return port->get_pin[pin];
}

int hal_gpio_port_count(void) {
	return GPIO_PORT_COUNT;
}

int hal_gpio_pin_count(void) {
	return GPIO_PIN_COUNT;
}

/*

gpio_base_t *gpioa;
gpio_base_t *gpiob;
gpio_base_t *gpioc;
gpio_base_t *gpiod;
gpio_base_t *gpioe;
gpio_base_t *gpiof;
gpio_base_t *gpiog;
static gpio_base_t **gpios[] = {&gpioa,&gpiob,&gpioc,&gpiod,&gpioe,&gpiof,&gpiog};
*/