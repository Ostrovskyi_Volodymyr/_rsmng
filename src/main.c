#include <kernel/gpio.h>
#include <kernel/timer.h>

int main(void) {
	gpio_t pin5 = gpio_request(5, GPIO_MODE_OUTPUT_PP);
	gpio_t pin44 = gpio_request(44, GPIO_MODE_OUTPUT_PP);

	gpio_set(&pin5, 0);
	gpio_set(&pin44, 0);
	while (1) {
		delay(500);
		gpio_switch(&pin5);
		gpio_switch(&pin44);
	}
}