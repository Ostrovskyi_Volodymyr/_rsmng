#include <kernel/gpio.h>

gpio_t gpio_request(int pin, mode_t mode) {
	gpio_t gpio;
	int _port = pin / (hal_gpio_pin_count() + 1);
	int _pin = pin - _port * hal_gpio_pin_count();
	gpio.port = hal_gpio_port(_port);
	gpio.pin = _pin;

	if (!hal_gpio_port_enabled(_port)) {
		hal_gpio_enable_port(_port);
	}

	switch (mode) {
		case GPIO_MODE_INPUT_PP:
			hal_gpio_config(gpio.port, gpio.pin, HAL_GPIO_MODE_INPUT_PP);
			break;
		case GPIO_MODE_OUTPUT_PP:
			hal_gpio_config(gpio.port, gpio.pin, HAL_GPIO_MODE_OUTPUT_PP | HAL_GPIO_OUT_SPEED_LOW);
			break;
	}
	return gpio;
}

void gpio_set(gpio_t *gpio, int value) {
	if (gpio->port == 0) {
		return;
	}
	hal_gpio_write(gpio->port, gpio->pin, (value == 0 ? 0 : 1));
}

int gpio_get(gpio_t *gpio) {
	if (gpio->port == 0) {
		return 0;
	}
	return hal_gpio_read(gpio->port, gpio->pin);
}

void gpio_switch(gpio_t *gpio) {
	int val = gpio_get(gpio);
	gpio_set(gpio, !val);
}