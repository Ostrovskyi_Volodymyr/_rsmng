#include <hal/gpio.h>
#include <hal/systick.h>
#include <hal/rcc.h>
#include <kernel/config.h>

void main();

unsigned int delay_ticks;

void startup_main(void) {
	pll_config_t pll_cfg;
	pll_cfg.mul = HAL_PLL_MUL_6;
	pll_cfg.hse_div = HAL_PLL_HSE_DIV_1;
	pll_cfg.src = HAL_PLL_SRC_HSI;
	hal_rcc_pll_config(&pll_cfg);
	hal_rcc_pll_enable();
	hal_rcc_sysclk_source(HAL_SYSCLK_SRC_PLL);
	unsigned int hclk_freq = hal_rcc_hclk_freq();
	hal_systick_set(hclk_freq / CLOCK_TICKS_PER_SEC);
	hal_systick_source(HAL_SYSTICK_SRC_AHB);
	hal_systick_enable(1);
	hal_gpio_init();
	main();
}

void irq_systick_handler(void) {
	delay_ticks++;
}