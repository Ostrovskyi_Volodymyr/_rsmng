#ifndef _HAL_GPIO_H_
#define _HAL_GPIO_H_

#include <hal/common.h>

typedef struct {
	struct {
		unsigned int mode[2];
		unsigned int cnf[2];
	} moder[16];
	unsigned int get_pin[32];
	unsigned int set_pin[32];
	unsigned int _bs[16];
	unsigned int _br[16];
	unsigned int _brr[32];
	unsigned int lock_pin[16];
	unsigned int lock_key;
	unsigned int __blank[15];
} gpio_base_t;

void hal_gpio_init(void);
gpio_base_t* hal_gpio_port(int port);
void hal_gpio_enable_port(int port);
void hal_gpio_disable_port(int port);
int hal_gpio_port_enabled(int port);
void hal_gpio_config(gpio_base_t *port, int pin, int mode);
void hal_gpio_write(gpio_base_t *port, int pin, int val);
int hal_gpio_read(gpio_base_t *port, int pin);
int hal_gpio_port_count(void);
int hal_gpio_pin_count(void);

#define HAL_GPIO_OUT_SPEED_LOW 0b0010
#define HAL_GPIO_OUT_SPEED_MID 0b0001
#define HAL_GPIO_OUT_SPEED_HIGH 0b0011
#define HAL_GPIO_MODE_OUTPUT_PP 0b0000
#define HAL_GPIO_MODE_OUTPUT_OD 0b0100
#define HAL_GPIO_MODE_AF_PP 0b1000
#define HAL_GPIO_MODE_AF_OD 0b1100
#define HAL_GPIO_MODE_INPUT_AG 0b0000
#define HAL_GPIO_MODE_INPUT_FL 0b0100
#define HAL_GPIO_MODE_INPUT_PP 0b1000

#endif // _HAL_GPIO_H_