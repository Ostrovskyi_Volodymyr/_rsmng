#ifndef _HAL_SYSTICK_H_
#define _HAL_SYSTICK_H_


#include <hal/common.h>

typedef struct {
	unsigned int ctrl;
	unsigned int load;
	unsigned int current;
	unsigned int calib;

} systick_base_t;

#define systick ((systick_base_t*)SYSTICK_BASE)

void hal_systick_enable(unsigned char intr);
void hal_systick_set(unsigned int value);
void hal_systick_source(int src);
unsigned int hal_systick_get(void);

#define HAL_SYSTICK_SRC_AHB_8 0x0
#define HAL_SYSTICK_SRC_AHB 0x1

#endif // _HAL_SYSTICK_H_