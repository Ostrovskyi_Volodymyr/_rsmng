#ifndef _HAL_RCC_H_
#define _HAL_RCC_H_

#include <hal/common.h>

typedef struct {
	unsigned int ctrl[32];
	unsigned int cfg[32];
	unsigned int ir[32];
	unsigned int apb2_rst[32];
	unsigned int apb1_rst[32];
	unsigned int ahb_en[32];
	unsigned int apb2_en[32];
	unsigned int apb1_en[32];
	unsigned int backup_ctrl[32];
	unsigned int status[32];
} rcc_base_t;

typedef struct {
	int mul;
	int hse_div;
	int src;
} pll_config_t;

#define rcc ((rcc_base_t*)PERIF_ALIAS_ADDR(RCC_BASE))

void hal_rcc_pll_config(pll_config_t *cfg);
void hal_rcc_pll_enable();
void hal_rcc_sysclk_source(int src);
unsigned int hal_rcc_hclk_freq(void);

#define HAL_PLL_MUL_2 0b0000
#define HAL_PLL_MUL_3 0b0001
#define HAL_PLL_MUL_4 0b0010
#define HAL_PLL_MUL_5 0b0011
#define HAL_PLL_MUL_6 0b0100
#define HAL_PLL_MUL_7 0b0101
#define HAL_PLL_MUL_8 0b0110
#define HAL_PLL_MUL_9 0b0111
#define HAL_PLL_MUL_10 0b1000
#define HAL_PLL_MUL_11 0b1001
#define HAL_PLL_MUL_12 0b1010
#define HAL_PLL_MUL_13 0b1011
#define HAL_PLL_MUL_14 0b1100
#define HAL_PLL_MUL_15 0b1101
#define HAL_PLL_MUL_16 0b1110

#define HAL_PLL_HSE_DIV_1 0x0
#define HAL_PLL_HSE_DIV_2 0x1

#define HAL_PLL_SRC_HSI 0x0
#define HAL_PLL_SRC_HSE 0x1

#define HAL_SYSCLK_SRC_HSI 0b00
#define HAL_SYSCLK_SRC_HSE 0b01
#define HAL_SYSCLK_SRC_PLL 0b10

#define HAL_HCLK_DIV_1 0b0000
#define HAL_HCLK_DIV_2 0b1000
#define HAL_HCLK_DIV_4 0b1001
#define HAL_HCLK_DIV_8 0b1010
#define HAL_HCLK_DIV_16 0b1011
#define HAL_HCLK_DIV_64 0b1100
#define HAL_HCLK_DIV_128 0b1101
#define HAL_HCLK_DIV_256 0b1110
#define HAL_HCLK_DIV_512 0b1111

#endif // _HAL_RCC_H_