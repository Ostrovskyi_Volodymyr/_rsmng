#ifndef _STDLIB_STRING_H_
#define _STDLIB_STRING_H_

void* memset(void * ptr, unsigned char value, unsigned int num);

#endif // _STDLIB_STRING_H_