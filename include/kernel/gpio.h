#ifndef _KERNEL_GPIO_H_
#define _KERNEL_GPIO_H_

#include <hal/gpio.h>

typedef enum {
	GPIO_MODE_INPUT_PP,
	GPIO_MODE_OUTPUT_PP,
} mode_t;

typedef struct {
	gpio_base_t *port;
	int pin;
	// TODO: fields
} gpio_t;

gpio_t gpio_request(int pin, mode_t mode);
void gpio_set(gpio_t *gpio, int value);
int gpio_get(gpio_t *gpio);
void gpio_switch(gpio_t *gpio);

#endif // _KERNEL_GPIO_H_